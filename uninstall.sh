#!/usr/bin/env bash

set -euxo pipefail

TMP_DIR=/tmp/nekuvi/

copy_files() {
    mkdir -p "$TMP_DIR"
    cp /usr/share/X11/xkb/rules/evdev.xml "$TMP_DIR"
}

remove_nekuvi_variant() {
    sed -i '/START nekuvi/,/END nekuvi/d' /usr/share/X11/xkb/symbols/np
}

remove_evdev_xml() {
    pushd "$TMP_DIR"
    sed -i '/START nekuvi/,/END nekuvi/d' evdev.xml
    popd
}

uninstall_evdev() {
    pushd "$TMP_DIR"
    cp evdev.* /usr/share/X11/xkb/rules/
    popd
}

process() {
    copy_files # copies the necessary files to a temp dir for us
    remove_nekuvi_variant # remove nekuvi variant from symbol file
    remove_evdev_xml # remove variant from evdev.xml
    uninstall_evdev
}


process
