#!/usr/bin/env python3

import sys
import xml.etree.ElementTree as ET
from xml.dom import minidom

filename = sys.argv[1]

root = ET.parse(filename)

languageList = ET.Element("languageList")
iso = ET.SubElement(languageList, "iso639Id")
iso.text = "nep"
iso.tail = "\n"
languageList.text = "\n"
languageList.tail = "\n"

configItem = ET.Element("configItem")
configItem.text = "\n"
configItem.tail = "\n"
name = ET.SubElement(configItem, "name")
name.text = "nekuvi"
name.tail = "\n"
shortDesc = ET.SubElement(configItem, "shortDescription")
shortDesc.text = "ne-nk"
shortDesc.tail = "\n"
desc = ET.SubElement(configItem, "description")
desc.text = "Nekuvi"
desc.tail = "\n"
configItem.append(languageList)

variant = ET.Element("variant")
variant.append(configItem)
variant.text = "\n"
variant.tail = "\n"

variantList = ET.Element("variantList")
variantList.append(variant)
variantList.text = "\n"
variantList.tail = "\n"

start_comment = ET.Comment("START nekuvi")
start_comment.tail = "\n"
end_comment = ET.Comment("END nekuvi")
end_comment.tail = "\n"

# find the nepali layout
layouts = root.findall(".//configItem/name[.='np']/../..")
if layouts:
    np_layout = layouts[0]
    np_layout.append(start_comment)
    np_layout.append(variantList)
    np_layout.append(end_comment)


xmlstr = minidom.parseString(ET.tostring(root.getroot(), "utf-8")).toprettyxml(
    newl="", indent=" " * 2
)

with open(filename, "w", encoding="utf-8") as f:
    f.write(xmlstr)
