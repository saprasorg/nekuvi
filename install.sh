#!/usr/bin/env bash

set -euxo pipefail

TMP_DIR=/tmp/nekuvi/

copy_files() {
    mkdir -p "$TMP_DIR"
    cp /usr/share/X11/xkb/symbols/np_prog "$TMP_DIR"
    cp /usr/share/X11/xkb/rules/evdev.xml "$TMP_DIR"
}


append_evdev_xml() {
    pushd "$TMP_DIR"

    if grep -q "np_prog" evdev.xml; then
        echo "WARNING: evdev.lst already updated. This could mean that you may have tried to install this package previously. Continuing installation."
    else
        /usr/share/libalpm/scripts/nekuvi-add-variant "$TMP_DIR"/evdev.xml
    fi
    popd
}

append_np() {
    pushd "$TMP_DIR"
    if grep -q "START nekuvi" evdev.xml; then
        echo "WARNING: evdev.lst already updated. This could mean that you may have tried to install this package previously. Continuing installation."
    else
        cat np_prog >> /usr/share/X11/xkb/symbols/np
        rm /usr/share/X11/xkb/symbols/np_prog
    fi
    popd
}



install_evdev() {
    pushd "$TMP_DIR"
    cp evdev.* /usr/share/X11/xkb/rules/
    popd
}

process() {
    copy_files # copies the necessary files to a temp dir for us
    append_np # append the symbol file np with the new variant
    append_evdev_xml # specify this variant in evdev.xml
    install_evdev
}


process
